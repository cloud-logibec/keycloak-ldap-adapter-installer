# Keycloak LDAP Adapter Installer

This project contains installation scripts to install the Azure Relay Proxy and the LDAP REST Api on windows.

After the installation, two Windows Services will be installed

- Logibec's Azure Relay Proxy
- Logibec's LDAP REST API

## Requirements

- Windows Server 2012 R2 and above
- Powershell 5.0 and above (For Windows Server 2012 you must install it - [download here](https://docs.microsoft.com/en-us/powershell/wmf/5.1/install-configure))
- Administrator priviledges
- Download prerequisites
    if you have difficulties downloading these files, you must add these sites in the trusted sites (https://cdn.azul.com, https://download.visualstudio.microsoft.com)

    - [Download and install latest .Net Core 8](https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/sdk-8.0.403-windows-x64-installer)
- Azure Cli Installed (used to download artifact)
- Personnal Access token (logibec-prod), Used to download artifact and access Azure Repos
- The Release version (Bundle Version for LDAP-Rest & Azure-Relay)
## Install

Execute the script below and follow the inscription, you must run Powershell as Administrator:

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls, [Net.SecurityProtocolType]::Tls11, [Net.SecurityProtocolType]::Tls12, [Net.SecurityProtocolType]::Ssl3; iex ((New-Object System.Net.WebClient).DownloadString('https://bitbucket.org/cloud-logibec/keycloak-ldap-adapter-installer/raw/HEAD/install.ps1'));
```


It is possible to install multiple instances of the services by adding a number at the end of the directory name:

| Directory Name                 | Service Names                           | Default API Port |
|--------------------------------|-----------------------------------------|------------------|
| AdConnector or  AdConnector1   | ldap-rest-api / azure-relay-proxy       | 8081             |
| AdConnector2                   | ldap-rest-api-2 / azure-relay-proxy-2   | 8082             |
| AdConnector10                  | ldap-rest-api-10 / azure-relay-proxy-10 | 8090             |