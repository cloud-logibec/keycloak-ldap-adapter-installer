[CmdletBinding()]
Param(
  [string] $nssmUrl = "https://nssm.cc/release",
  [string] $nssmVersion = "2.24",
  [string] $RELEASEVERSION = "",
  [string] $AzureArtifactPAT = ""
)

[Net.ServicePointManager]::Expect100Continue = $true;
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls, [Net.SecurityProtocolType]::Tls11, [Net.SecurityProtocolType]::Tls12, [Net.SecurityProtocolType]::Ssl3;

[void][System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms")

## Prerequisite
## ## DotNet XXX
##
##
##
##
$global:AzureArtifactPat=$AzureArtifactPAT
$global:RELEASEVERSION=$RELEASEVERSION
$AzureArtifactOrg="https://dev.azure.com/logibec-prod"
$AzureArtifactProject="connex"
$AzureArtifactFeed="connex_adapter"
$AzureRepo="connex-adapter-versions-prod"
##################
## Functions
##################
function Get-CleanedText([string] $value) {
  $cleanedValue = "";
  foreach ($c in $value.ToCharArray()) {
    if ([int]$c -ne 0) {
      $cleanedValue += [string] $c;
    }
  }
  $cleanedValue.Replace("`r`n", "`n").Trim()
}

function Invoke-Command ($commandTitle, $commandPath, $commandArguments) {
  Try {
    $pinfo = New-Object System.Diagnostics.ProcessStartInfo
    $pinfo.FileName = $commandPath
    $pinfo.RedirectStandardError = $true
    $pinfo.RedirectStandardOutput = $true
    $pinfo.UseShellExecute = $false
    $pinfo.Arguments = $commandArguments
    $p = New-Object System.Diagnostics.Process
    $p.StartInfo = $pinfo
    $p.Start() | Out-Null

    [pscustomobject]@{
      commandTitle = $commandTitle
      stdout       = Get-CleanedText -value $p.StandardOutput.ReadToEnd()
      stderr       = Get-CleanedText -value $p.StandardError.ReadToEnd()
      ExitCode     = $p.ExitCode
    }
    $p.WaitForExit()
  }
  Catch {
    exit
  }
}
Function Invoke-AzureArtifact{ 
 param ([string]$org,[string]$project,[string]$feed,[string]$artifact,[string]$Version)
 $token=$global:AzureArtifactPAT
$env:AZURE_DEVOPS_EXT_PAT="$global:AzureArtifactPAT"
start-process -filePath "az" -ArgumentList "artifacts","universal","download","--organization","$AzureArtifactOrg","--project","$AzureArtifactProject","--scope","project","--feed","$AzureArtifactFeed","--name $artifact","--version","$Version","--path","." -wait -NoNewWindow
}


function Read-HostWithDefault([string] $text, [string] $defaultValue) {

  $msg = $text;

  if (-not ([string]::IsNullOrEmpty($defaultValue))) {
    $msg += " [$($defaultValue)]"
  }

  $value = Read-Host $msg

  if (-not ([string]::IsNullOrEmpty($value))) {
    $value
  }
  else {
    $defaultValue
  }
}

function Get-ServiceEnvVar ([string] $name, [string] $key) {
  $result = Invoke-Command -commandTitle "nssm get environment variables" -commandPath $nssmPath -commandArguments "get $($name) AppEnvironmentExtra $($key)"
  if (-not ([string]::IsNullOrEmpty($result.stdout))) {
    $result.stdout
  }
  else {
    Out-Null
  }
}

function Get-ServiceParams ([string] $name) {
  $result = Invoke-Command -commandTitle "nssm get app parameters" -commandPath $nssmPath -commandArguments "get $($name) AppParameters"
  if (-not ([string]::IsNullOrEmpty($result.stdout))) {
    $result.stdout
  }
  else {
    Out-Null
  }
}

# Get next available port (next port on which a listen is possible), starting at give port number
function Get-NextAvailablePort([int] $port)
{
  while($true)
  {
    trap [System.Net.Sockets.SocketException]
    {
      $script:isInUse = $TRUE;
      continue;
    }
    $script:isInUse=$FALSE
    $listener = New-Object System.Net.Sockets.TcpListener("127.0.0.1", $port)
    $listener.Start()
    if ($script:isInUse)
    {
      $port = $port + 1
    }
    else
    {
      $listener.Stop()
      break
    }
  }
  return $port
}

# Compute a suffix for service name, based on current directory
# It is done by removing the 'by convention' prefix from current location
function Get-ServiceSuffix([string] $currentLocation)
{

  $result = $currentLocation -ireplace "ad-connector-", ""
  $result = $result -ireplace "ad-connector", ""
  $result = $result -ireplace "adconnector-", ""
  $result = $result -ireplace "adconnector", ""
  return $result
}


##################
## END: Functions
##################

##################
## Get nssm and install directory
##################
$location = Get-Location

if ($location -match "\s+") {
  Write-Host "Error the location $($location) contains a space."
  exit
}

if (-not (Test-Path -Path nssm)) {

  $install = Read-Host "Do you want install in the current directory? [Y/n]";
  if ($null -eq $install) {
    $install = "Y"
  }

  $location = Get-Location;
  if ($install.ToString().ToUpper() -eq "N") {
    $fileBrowser = New-Object System.Windows.Forms.FolderBrowserDialog;
    if ($fileBrowser.ShowDialog() -eq "OK") {
      $location = $fileBrowser.SelectedPath;
      Set-Location $location
    }
  }

  Write-Host "Downloading nssm $($nssmVersion)";
  Invoke-WebRequest -UseBasicParsing -Uri $nssmUrl/nssm-$nssmVersion.zip -OutFile nssm-$nssmVersion.zip
  Expand-Archive -Path nssm-$nssmVersion.zip -DestinationPath tmp
  Move-Item -Path tmp/nssm-$nssmVersion -Destination nssm
  Remove-Item tmp -Force
  Remove-Item nssm-$nssmVersion.zip -Force
}

$nssmPath = Join-Path -Path $location -ChildPath "nssm\win32\nssm.exe"
if ([Environment]::Is64BitProcess -eq [Environment]::Is64BitOperatingSystem) {
  $nssmPath = Join-Path -Path $location -ChildPath "nssm\win64\nssm.exe"
}
##################
## END: Get nssm and install directory
##################

###################
## Install ldap-rest-api
###################
$ldapName = "ldap-rest-api"
$ldapExt = ".jar"
$ldapTitle = "Logibec's LDAP REST API"
$ldapDesc = "Service which connect to an LDAP server"
$ldapRelayEndpoint = "ldap-relay/api/v2"
$configurationPath = "$($ldapRelayEndpoint)/configurations"
$asyncSearchResultsPath = "$($ldapRelayEndpoint)/sync-results"

$currentDir = Split-Path -Path ($location) -Leaf 
$serviceSuffix = Get-ServiceSuffix $currentDir
$ldapServiceName = "$($ldapName)"
$ldapDefaultPort = Get-NextAvailablePort 8080

# To support legacy 'AdConnector' naming we check we actually have a suffix
if ($serviceSuffix -ne "") {
  $ldapServiceName = "$($ldapName)-$serviceSuffix"
  $ldapTitle = "$($ldapTitle) ($serviceSuffix)"
}

function Get-LdapRestApi {
Get-Java
  if (-not (Test-Path "$($ldapName)$($ldapExt)")) {
  $url = "$AzureArtifactOrg/$AzureArtifactProject/_apis/git/repositories/$AzureRepo/items?path=/versions.yml&versionDescriptor.version=release/$global:ReleaseVersion&versionDescriptor.versionType=branch&api-version=6.0"
  $response = Invoke-RestMethod -Uri $url -Headers @{Authorization = "Basic $([Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(":$global:AzureArtifactPAT")))"}
  $lines = $response -split "`n"
  $LdapVersion = (($lines | Where-object { $_ -like "*ANSIBLE_VAR_LDAP_RELAY_VERSION*" }) -split ":")[1] -replace " ","" -replace "`n","" -replace "`r",""
  $JAVA_VERSION = (($lines | Where-object { $_ -like "*ANSIBLE_VAR_LDAP_RELAY_JDK_VERSION*" }) -split ":")[1] -replace " ","" -replace "`n","" -replace "`r",""
  Write-Host $JAVA_VERSION
    Invoke-AzureArtifact -org $AzureArtifactOrg -project $AzureArtifactProject -feed $AzureArtifactFeed -artifact "ldap-rest-api" -version "$LdapVersion"
    Move-Item -Path "$($ldapName)-$($ldapVersion)$($ldapExt)" -Destination "$($ldapName)$($ldapExt)"
    Set-Content -Path "$($ldapName).txt" -Value $ldapVersion
    $ldapExe = Join-Path -Path $location -ChildPath "$($ldapName)$($ldapExt)"
    Set-Content -Path "$($ldapName).bat" -Value "jdk$JAVA_VERSION\bin\java %JAVA_OPTS% -jar $($ldapExe) %*"
  }

}
Function get-java {

  $url = "$AzureArtifactOrg/$AzureArtifactProject/_apis/git/repositories/$AzureRepo/items?path=/versions.yml&versionDescriptor.version=release/$global:ReleaseVersion&versionDescriptor.versionType=branch&api-version=6.0"
  $response = Invoke-RestMethod -Uri $url -Headers @{Authorization = "Basic $([Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(":$global:AzureArtifactPAT")))"}
  $lines = $response -split "`n"
  $JAVA_VERSION = (($lines | Where-object { $_ -like "*ANSIBLE_VAR_LDAP_RELAY_JDK_VERSION*" }) -split ":")[1] -replace " ","" -replace "`n","" -replace "`r",""

if (-not (Test-Path jdk$JAVA_VERSION\bin\java.exe)) {

if ((gci -Filter "zulu*$JAVA_VERSION*win*.zip" | Measure-Object).Count -eq 0) { 
Invoke-AzureArtifact -org $AzureArtifactOrg -project $AzureArtifactProject -feed $AzureArtifactFeed -artifact "jdk-windows" -version "$JAVA_VERSION"
}

$JDKFILE=(gci -Filter "zulu*$JAVA_VERSION*win*.zip").Name
    Expand-Archive -Path "$JDKFILE" -DestinationPath "jdk$JAVA_VERSION"
    Move-Item -Path "jdk$JAVA_VERSION\zulu*\*" -Destination "jdk$JAVA_VERSION"
    Remove-item $JDKFILE

}
}
function Check-Prereq {
if (!(Get-Command "az" -ErrorAction SilentlyContinue)) {
Write-Host "AZ Cli not installed"
Exit
}
if (-not $global:AzureArtifactPat) {
Write-Host "PAT Empty"
$global:AzureArtifactPat = Read-Host "Enter your PAT"
} 
if (-not $global:RELEASEVERSION) {
Write-Host "ReleaseVersion  Empty"
$global:RELEASEVERSION = Read-Host "Enter the Version"
} 

}
Check-Prereq
$result = Invoke-Command -commandTitle "nssm status" -commandPath $nssmPath -commandArguments "status $($ldapServiceName)"  
$action = "0"


if ($result.ExitCode -ne 0) {
  Get-LdapRestApi

  Invoke-Command -commandTitle "nssm install" -commandPath $nssmPath -commandArguments "install $($ldapServiceName) $(Join-Path -Path $location -ChildPath $($ldapName)).bat"
  Invoke-Command -commandTitle "nssm set title" -commandPath $nssmPath -commandArguments "set $($ldapServiceName) DisplayName $($ldapTitle)"
  Invoke-Command -commandTitle "nssm set description" -commandPath $nssmPath -commandArguments "set $($ldapServiceName) Description $($ldapDesc)"
  New-Item -ItemType Directory -Force -Path $(Join-Path -Path $location -ChildPath "logs\$($ldapName)")
  Invoke-Command -commandTitle "nssm log stdout" -commandPath $nssmPath -commandArguments "set $($ldapServiceName) AppStdout $(Join-Path -Path $location -ChildPath "logs\$($ldapName)\out.log")"
  Invoke-Command -commandTitle "nssm log stderr" -commandPath $nssmPath -commandArguments "set $($ldapServiceName) AppStderr $(Join-Path -Path $location -ChildPath "logs\$($ldapName)\error.log")"
  Invoke-Command -commandTitle "nssm log rotate" -commandPath $nssmPath -commandArguments "set $($ldapServiceName) AppRotateFiles 1"
  Invoke-Command -commandTitle "nssm log rotate" -commandPath $nssmPath -commandArguments "set $($ldapServiceName) AppRotateOnline 1"

  $action = "1"
}
else {
  Write-Host "Action to perform for $($ldapServiceName)"
  Write-Host "1. Update configuration"
  Write-Host "2. Update application"
  Write-Host "3. Remove service"
  Write-Host "4. Continue to AZURE RELAY PROXY"
  $action = Read-Host "Enter the number of the action"
}

if ($action -eq "1") {
  Write-Host "Configure $($ldapServiceName):"
  $hasChanges = $false

  $apiPasswordEncryptionKeyValue = Get-ServiceEnvVar -name $ldapServiceName -key API_PASSWORD_ENCRYPTION_KEY
  if ($null -eq $apiPasswordEncryptionKeyValue) {
    $apiPasswordEncryptionKeyValue = -join ((48..57) + (97..122) | Get-Random -Count 32 | ForEach-Object { [char]$_ })
  }
  $apiPasswordEncryptionKey = $apiPasswordEncryptionKeyValue
  if ($apiPasswordEncryptionKey -ne $apiPasswordEncryptionKeyValue) { $hasChanges = $true }

  $javaOptsValue = Get-ServiceEnvVar -name $ldapServiceName -key JAVA_OPTS
  $javaOpts = Read-HostWithDefault -text "Java options (optional)" -defaultValue "$javaOptsValue"
  if ($javaOpts -ne $javaOptsValue) { $hasChanges = $true }

  $cmdArgsValue = Get-ServiceParams -name $ldapServiceName
  $cmdArgs = Read-HostWithDefault -text "Application parameters (optional)" -defaultValue $cmdArgsValue
  if ($cmdArgs -ne $cmdArgsValue) {
    Invoke-Command -commandTitle "nssm set app parameters" -commandPath $nssmPath -commandArguments "set $($ldapServiceName) AppParameters `"$($cmdArgs)`""
  }

  # Relay ID - $relayId
  $apiRelayIdValue = Get-ServiceEnvVar -name $ldapServiceName -key API_RELAY_ID
if ($serviceSuffix -ne "") {
$apiRelayId = "$(hostname)-$serviceSuffix"
} else {
$apiRelayId = "$(hostname)"

}


  if ($apiRelayIdValue -ne $apiRelayId) { $hasChanges = $true }

  # High-Availability

  $apiClientIdValue = Get-ServiceEnvVar -name $ldapServiceName -key API_CLIENT_ID
  $apiClientId = "keycloak-ldap-relay-adapter"
  if ($apiClientIdValue -ne $apiClientId) { $hasChanges = $true }

  $apiClientSecretValue = Get-ServiceEnvVar -name $ldapServiceName -key API_CLIENT_SECRET
  $apiClientSecret = Read-HostWithDefault -text "Keycloak > Client Secret" -defaultValue $apiClientSecretValue
  if ($apiClientSecretValue -ne $apiClientSecret) { $hasChanges = $true }

  $keycloakHostValue = Get-ServiceEnvVar -name $ldapServiceName -key KEYCLOAK_HOST
  $keycloakHost = Read-HostWithDefault -text "Keycloak Host (https://...)" -defaultValue $keycloakHostValue
  if ($keycloakHostValue -ne $keycloakHost) { $hasChanges = $true }

  $keycloakRealmValue = Get-ServiceEnvVar -name $ldapServiceName -key KEYCLOAK_REALM
  $keycloakRealm = Read-HostWithDefault -text "Keycloak Realm" -defaultValue $keycloakRealmValue
  if ($keycloakRealmValue -ne $keycloakRealm) { $hasChanges = $true }

  # LDAP Config
  $ldapReferralIgnoreValue = Get-ServiceEnvVar -name $ldapServiceName -key LDAP_REFERRAL_IGNORE
  if ($null -eq $ldapReferralIgnoreValue) { $ldapReferralIgnoreValue = "True" }
  $ldapReferralIgnore = Read-HostWithDefault -text "LDAP Referral Ignore ?" -defaultValue $ldapReferralIgnoreValue
  if ($ldapReferralIgnoreValue -ne $ldapReferralIgnore) { $hasChanges = $true }

  # Ldap API port
  $ldapPortValue =  Get-ServiceEnvVar -name $ldapServiceName -key SERVER_PORT
  if ($null -eq $ldapPortValue) { $ldapPortValue = $ldapDefaultPort }
  $ldapPort = Read-HostWithDefault -text "LDAP rest API port" -defaultValue $ldapPortValue
  if ($ldapPortValue -ne $ldapPort) { $hasChanges = $true }

  Write-Host ""
  Write-Host "Do you want to force apply?"
  $forceApply = Read-HostWithDefault -text "force apply?" -defaultValue $hasChanges

  if ($hasChanges -Or $forceApply -eq $true) {
    $apiTokenEndpoint = "$($keycloakHost)/auth/realms/$($keycloakRealm)/protocol/openid-connect/token"
    $apiConfigurationPath = "$($keycloakHost)/auth/realms/$($keycloakRealm)/$($configurationPath)"
    $apiasyncSearchResultsPath = "$($keycloakHost)/auth/realms/$($keycloakRealm)/$($asyncSearchResultsPath)"

    $environmentArray = @{
      "API_PASSWORD_ENCRYPTION_KEY"       = "$($apiPasswordEncryptionKey)"
      "JAVA_OPTS"                         = "$($javaOpts)"
      "API_CLIENT_ID"                     = "$($apiClientId)"
      "API_CLIENT_SECRET"                 = "$($apiClientSecret)"
      "API_RELAY_ID"                      = "$($apiRelayId)"
      "KEYCLOAK_HOST"                     = "$($keycloakHost)"
      "KEYCLOAK_REALM"                    = "$($keycloakRealm)"
      "API_TOKEN_ENDPOINT"                = "$($apiTokenEndpoint)"
      "API_CONFIGURATION_ENDPOINT"        = "$($apiConfigurationPath)"
      "API_ASYNC_SEARCH_RESULTS_ENDPOINT" = "$($apiasyncSearchResultsPath)"
      "LDAP_REFERRAL_IGNORE"              = "$($ldapReferralIgnore)"
      "SERVER_PORT"                       = "$($ldapPort)"
    }

    $environmentExtra = ""

    $environmentArray.GetEnumerator() | ForEach-Object {
      if (![string]::IsNullOrEmpty($_.value)) {
        $environmentExtra += " ""$($_.key)=$($_.value)"""
      }
    }

    Write-Host "Setting AppEnvironmentExtra for $($ldapServiceName): $($environmentExtra)"
    Invoke-Command -commandTitle "nssm set environment variables" -commandPath $nssmPath -commandArguments "set $($ldapServiceName) AppEnvironmentExtra $($environmentExtra)"

    Write-Host "Restarting $($ldapServiceName)"
    $result = Invoke-Command -commandTitle "nssm restart" -commandPath $nssmPath -commandArguments "restart $($ldapServiceName)"
    if ($result.ExitCode -ne 0) {
      Write-Error $result.stderr
    }
  }
}

if ($action -eq "2") {

  Write-Host "Stopping $($ldapServiceName)"
  Invoke-Command -commandTitle "nssm stop" -commandPath $nssmPath -commandArguments "stop $($ldapServiceName)"
  $backupFiles = Get-ChildItem -Path "$($ldapName)$($ldapExt).*"
  foreach ($file in $backupFiles) {
    $fileName = $file.Name
    Write-Host "$($fileName)"
    Write-Host "Removing $($fileName)"
    Remove-Item -Path "$($fileName)" -Force -Recurse
  }

  $date = Get-Date -Format "yyyy-MM-dd"
  if (Test-Path -Path "$($ldapName)$($ldapExt)") {
    Write-Host "Renaming $($ldapName)$($ldapExt) to $($ldapName)$($ldapExt).$($date)"
    Rename-Item -Path "$($ldapName)$($ldapExt)" -NewName "$($ldapName)$($ldapExt).$($date)" -Force
  }

  Write-Host "Getting $($ldapName)"
  Get-LdapRestApi
  Write-Host "Starting $($ldapName)"
  Invoke-Command -commandTitle "nssm start" -commandPath $nssmPath -commandArguments "start $($ldapServiceName)"
  if ($result.ExitCode -ne 0) {
    Write-Error $result.stderr
  }
  else {
    Write-Host "Removing backup $($ldapName)$($ldapExt).$($date)"
    Remove-Item -Path "$($ldapName)$($ldapExt).$($date)"
  }
}

if ($action -eq "3") {
  Invoke-Command -commandTitle "nssm stop" -commandPath $nssmPath -commandArguments "stop $($ldapServiceName)"
  Invoke-Command -commandTitle "nssm remove" -commandPath $nssmPath -commandArguments "remove $($ldapServiceName) confirm"
}
###################
## END: Install ldap-rest-api
###################

##################
## Install azure-relay-proxy
##################
$proxyName = "azure-relay-proxy"
$proxyTitle = "Logibec's Azure Relay Proxy"
$proxyDesc = "Service which act as a Proxy between Azure Relay and Logibec's LDAP REST API"
$proxyExt = ".zip"
$protocolePath = "protocol/openid-connect/token"
$pluginMetricsName = "$($ldapRelayEndpoint)/metrics"

$proxyServiceName = "$($proxyName)"
# To support legacy 'AdConnector' naming we check we actually have a suffix
if ($serviceSuffix -ne "") {
  $proxyServiceName = "$($proxyName)-$serviceSuffix"
  $proxyTitle = "$($proxyTitle) ($serviceSuffix)"
}

function Get-AzureRelayProxy {
  $url = "$AzureArtifactOrg/$AzureArtifactProject/_apis/git/repositories/$AzureRepo/items?path=/versions.yml&versionDescriptor.version=release/$global:ReleaseVersion&versionDescriptor.versionType=branch&api-version=6.0"
  $response = Invoke-RestMethod -Uri $url -Headers @{Authorization = "Basic $([Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(":$global:AzureArtifactPAT")))"}
  $lines = $response -split "`n"
  $proxyVersion = (($lines | Where-object { $_ -like "*ANSIBLE_VAR_AZURE_RELAY_VERSION*" }) -split ":")[1] -replace " ","" -replace "`n","" -replace "`r",""
if (-not (Test-Path "$($proxyName)\$($proxyVersion).txt")) {
  if (-not (Test-Path "$($proxyName)-$($proxyVersion)$($proxyExt)")) {
     Invoke-AzureArtifact -org $AzureArtifactOrg -project $AzureArtifactProject -feed $AzureArtifactFeed -artifact "azure-relay-proxy" -version "$proxyVersion"
}

    Expand-Archive -Path "$($proxyName)-$($proxyVersion)$($proxyExt)" -DestinationPath "$($proxyName)"
    Remove-Item -Path "$($proxyName)-$($proxyVersion)$($proxyExt)"
    Set-Content -Path "$($proxyName).txt" -Value $proxyVersion
    Set-Content -Path "$($proxyName)\$($proxyVersion).txt" -Value $proxyVersion

    Set-Content -Path "$($proxyName).bat" -Value "dotnet $(Join-Path -Path $location -ChildPath "$($proxyName)\Azure.Relay.Proxy.App.dll")"
  }
}

# check if service is installed
$result = Invoke-Command -commandTitle "nssm status" -commandPath $nssmPath -commandArguments "status $($proxyServiceName)"
$action = "0"

if ($result.ExitCode -ne 0) {
  Get-AzureRelayProxy
  Invoke-Command -commandTitle "nssm install" -commandPath $nssmPath -commandArguments "install $($proxyServiceName) $(Join-Path -Path $location -ChildPath $($proxyName)).bat"
  Invoke-Command -commandTitle "nssm set title" -commandPath $nssmPath -commandArguments "set $($proxyServiceName) DisplayName $($proxyTitle)"
  Invoke-Command -commandTitle "nssm set description" -commandPath $nssmPath -commandArguments "set $($proxyServiceName) Description $($proxyDesc)"
  New-Item -ItemType Directory -Force -Path $(Join-Path -Path $location -ChildPath "logs\$($proxyName)")
  Invoke-Command -commandTitle "nssm log stdout" -commandPath $nssmPath -commandArguments "set $($proxyServiceName) AppStdout $(Join-Path -Path $location -ChildPath "logs\$($proxyName)\out.log")"
  Invoke-Command -commandTitle "nssm log stderr" -commandPath $nssmPath -commandArguments "set $($proxyServiceName) AppStderr $(Join-Path -Path $location -ChildPath "logs\$($proxyName)\error.log")"
  Invoke-Command -commandTitle "nssm log rotate" -commandPath $nssmPath -commandArguments "set $($proxyServiceName) AppRotateFiles 1"
  Invoke-Command -commandTitle "nssm log rotate" -commandPath $nssmPath -commandArguments "set $($proxyServiceName) AppRotateOnline 1"
  $action = "1"
}
else {
  Write-Host "Action to perform for $($proxyServiceName)"
  Write-Host "1. Update configuration"
  Write-Host "2. Update application"
  Write-Host "3. Remove service"
  Write-Host "4. Quit installation"
  $action = Read-Host "Enter the number of the action"
}

if ($action -eq "1") {
  Write-Host "Configure Azure Relay:"
  $hasChanges = $false
  $relayNamespaceValue = Get-ServiceEnvVar -name $proxyServiceName -key RELAY_NAMESPACE
  $relayNamespace = Read-HostWithDefault -text "Relay Namespace > Name" -defaultValue $relayNamespaceValue
  if ($relayNamespaceValue -ne $relayNamespace) { $hasChanges = $true }

  $relayNameValue = Get-ServiceEnvVar -name $proxyServiceName -key RELAY_NAME
  $relayName = Read-HostWithDefault -text "Hybrid Connection > Name" -defaultValue $relayNameValue
  if ($relayNameValue -ne $relayName) { $hasChanges = $true }

  $relayKeyNameValue = Get-ServiceEnvVar -name $proxyServiceName -key RELAY_KEY_NAME
  $relayKeyName = Read-HostWithDefault -text "Shared Access Policy > Listener Name" -defaultValue $relayKeyNameValue
  if ($relayKeyNameValue -ne $relayKeyName) { $hasChanges = $true }

  $relayKeyValue = Get-ServiceEnvVar -name $proxyServiceName -key RELAY_KEY
  $relayKey = Read-HostWithDefault -text "Shared Access Policy > Listener Key" -defaultValue $relayKeyValue
  if ($relayKeyValue -ne $relayKey) { $hasChanges = $true }

  $relayIdValue = Get-ServiceEnvVar -name $proxyServiceName -key RELAY_ID
if ($serviceSuffix -ne "") {
$relayId = "$(hostname)-$serviceSuffix"
} else {
$relayId = "$(hostname)"


  }
  if ($relayId -ne $relayIdValue) { $hasChanges = $true }


  $relayClientIdValue = "keycloak-ldap-relay-adapter"
  $relayClientId = $relayClientIdValue
  if ($relayClientIdValue -ne $relayClientId) { $hasChanges = $true }

  $relayClientSecretValue = Get-ServiceEnvVar -name $proxyServiceName -key RELAY_CLIENT_SECRET
  $relayClientSecret = Read-HostWithDefault -text "Keycloak > Client Secret" -defaultValue $relayClientSecretValue
  if ($relayClientSecretValue -ne $relayClientSecret) { $hasChanges = $true }

  $keycloakHostValue = Get-ServiceEnvVar -name $proxyServiceName -key KEYCLOAK_HOST
  $keycloakHost = Read-HostWithDefault -text "Keycloak Host (https://...)" -defaultValue $keycloakHostValue
  if ($keycloakHostValue -ne $keycloakHost) { $hasChanges = $true }

  $keycloakRealmValue = Get-ServiceEnvVar -name $proxyServiceName -key KEYCLOAK_REALM
  $keycloakRealm = Read-HostWithDefault -text "Keycloak Realm" -defaultValue $keycloakRealmValue
  if ($keycloakRealmValue -ne $keycloakRealm) { $hasChanges = $true }

  $apiPortValue = Get-ServiceEnvVar -name $proxyServiceName -key RELAY_API_PORT
  if ($null -eq $apiPortValue) { $apiPortValue = $ldapPort }
  if ($null -eq $apiPortValue) { $apiPortValue = $ldapDefaultPort }
  $apiPort = Read-HostWithDefault -text "LDAP rest API port" -defaultValue $apiPortValue
  if ($apiPort -ne $apiPortValue) { $hasChanges = $true }

  $proxyValue = Get-ServiceEnvVar -name $proxyServiceName -key RELAY_PROXY
  if ($null -eq $proxyValue) { $proxyValue = Get-ServiceEnvVar -name $proxyServiceName -key XPROXY }
  $proxy = Read-HostWithDefault -text "HTTP Proxy" -defaultValue $proxyValue
  if ($proxyValue -ne $proxy) { $hasChanges = $true }

  Write-Host ""
  Write-Host "Do you want to force apply?"
  $forceApply = Read-HostWithDefault -text "force apply?" -defaultValue $hasChanges

  $relayTokenEndpoint = "$($keycloakHost)/auth/realms/$($keycloakRealm)/$($protocolePath)"
  $relayMetricsPostUri = "$($keycloakHost)/auth/realms/$($keycloakRealm)/$($pluginMetricsName)"
  if ($hasChanges -Or $forceApply -eq $true) {
    Write-Host "Setting environment variables for $proxyName"
    Invoke-Command -commandTitle "nssm set environment variables" -commandPath $nssmPath -commandArguments "set $($proxyServiceName) AppEnvironmentExtra RELAY_NAMESPACE=$($relayNamespace) RELAY_NAME=$($relayName.ToLower()) RELAY_KEY_NAME=$($relayKeyName) RELAY_KEY=$($relayKey) RELAY_ID=$($relayId) RELAY_METRICS_ENDPOINT=$($relayMetricsPostUri) RELAY_CLIENT_ID=$($relayClientId) RELAY_CLIENT_SECRET=$($relayClientSecret) RELAY_TOKEN_ENDPOINT=$($relayTokenEndpoint) KEYCLOAK_HOST=$($keycloakHost) KEYCLOAK_REALM=$($keycloakRealm) RELAY_API_PORT=$($apiPort) RELAY_PROXY=$($proxy)"

    Write-Host "Restarting $proxyName"
    $result = Invoke-Command -commandTitle "nssm restart" -commandPath $nssmPath -commandArguments "restart $($proxyServiceName)"
    if ($result.ExitCode -ne 0) {
      Write-Error $result.stderr
    }
  }
}

if ($action -eq "2") {
  Write-Host "Stopping $proxyServiceName"
  Invoke-Command -commandTitle "nssm stop" -commandPath $nssmPath -commandArguments "stop $($proxyServiceName)"
  $date = Get-Date -Format "yyyy-MM-dd"
  $backupFolders = Get-ChildItem -Directory -Path "$($proxyName).*"
  foreach ($folder in $backupFolders) {
    $folderName = $folder.Name
    Write-Host "$($folderName)"
    Remove-Item -Path "$($folderName)" -Force -Recurse
  }

  $proxyFolder = Get-ChildItem -Directory -Path "$($proxyName)" -ErrorAction SilentlyContinue
  if ($null -ne $proxyFolder) {
    Write-Host "Renaming $($proxyName) to $($proxyName).$($date)"
    Rename-Item -Path "$($proxyName)" -NewName "$($proxyName).$($date)" -Force
  }

  Write-Host "Getting $proxyName"
  Get-AzureRelayProxy
  Write-Host "Starting $proxyName"
  $result = Invoke-Command -commandTitle "nssm start" -commandPath $nssmPath -commandArguments "start $($proxyServiceName)"
  if ($result.ExitCode -ne 0) {
    Write-Error $result.stderr
  }
  else {
    if (Test-Path -Path "$($proxyName).$($date)") {
      Write-Host "Removing backup $($proxyName).$($date)"
      Remove-Item -Path "$($proxyName).$($date)" -Force -Recurse
    }
  }
}

if ($action -eq "3") {

  Write-Host "Stopping $proxyServiceName"
  Invoke-Command -commandTitle "nssm stop" -commandPath $nssmPath -commandArguments "stop $($proxyServiceName)"
  Write-Host "Removing $proxyName"
  Invoke-Command -commandTitle "nssm remove" -commandPath $nssmPath -commandArguments "remove $($proxyServiceName) confirm"
}
##################
## END: Install azure-relay-proxy
##################